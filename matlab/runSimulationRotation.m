function runSimulationRotation()
configurePath();
loadGurobi();
yalmip('clear')
warning('off','all');


clear all; close all; clc
global experimentNumber
experimentNumber = '3';
T = 0.06;
zCoM = 0.24;
% zCoM = 0.8;
footDimensions = struct;
% footDimensions.length = 0.24;
% footDimensions.width = 0.14;
footDimensions.length = 0.055;
footDimensions.width = 0.025;
% vxmax = 6;
% vymax = 2;
vxmax = 0.6;
vymax = 0.4;
% vxmax = 1.4;
% vymax = 1.4;
% N = 16;
N = 10;
Nf = 3;
% Nf = 7;
Nr = 11;
% Nr = 17;
Nstep = 5;
% Nstep = 8;
alpha = 10^-6;
beta = 1;
% gamma = 50;
gamma = 10^-4;
ySep = 0.11;
% ySep = 0.2;
xc = 0;
yc = ySep;
thetac = 0;
x = [0; 0; 0];
y = [ySep; 0; 0];
xa = 0;
ya = 0;
dxref = 0.2;
% dxref = 05;
dyref = 0;
dtheta = 0;
% dtheta = -0.4;
isLeftSwing = false;
Ncon = 18;
% Ncon = 3;
Nprev = 5;
Nsim = N * Ncon * Nprev;
axmax = 2;
aymax = 2;
% axmax = 4;
% aymax = 4;
dthetaMax = 2.0 * (2.0 * pi);

config.Ncon = Ncon;
config.title = 'Push from the right while rotating -- x1/6';
% config.title = 'Push from the right while rotating -- x1/6';
% config.title = 'Push from the front while walking straight';% -- x1/6';
simulationParams.state0 = [x; y];
simulationParams.xc = xc;
simulationParams.yc = yc;
simulationParams.thetac = thetac;
simulationParams.xa = xa;
simulationParams.ya = ya;
simulationParams.thetaa = 0;
simulationParams.isLeftSwing = isLeftSwing;

disturbances(1).kd = 2 * N * Ncon + 1;% - Nstep * Ncon; % used for showing the rotation heuristics
disturbances(1).time = disturbances(1).kd * T / Ncon;
% disturbances(1).xDist = [0; -0.06; 0];
% disturbances(1).xDist = [0; -0.1; 0];
% disturbances(1).xDist = [0; -0.1; 0];
% disturbances(1).xDist = [0; 0.0; 0];
% disturbances(1).yDist = [0; 0.041; 0]; % right, not rotating
% disturbances(1).xDist = [0; -0.195; 0]; % very interesting, many steps to recover (rotating)
% disturbances(1).xDist = [0.0; -0.245; 0]; % very interesting, many steps to recover
disturbances(1).xDist = [0.0; -0.2; 0]; % very interesting, many steps to recover
% disturbances(1).xDist = [0.0; -1.14; 0]; % hrp - very interesting, many steps to recover
% disturbances(1).xDist = [0; 0.0; 0];
% disturbances(1).xDist = [0; -0.15; 0];
% disturbances(1).yDist = [0; 0.145; 0];
disturbances(1).yDist = [0; -0.0; 0];
% disturbances(1).yDist = [0; 0.22; 0];
% disturbances(1).yDist = [0; -0.1; 0];
% disturbances(1).yDist = [0; -0.235; 0];
% disturbances(1).yDist = [0; 0.16; 0];
% disturbances(1).yDist = [0; -0.05; 0];

delta = 10;
xi = 10;
% controller = YalmipController(T, zCoM, N, Nf, Nstep,...
%                 vxmax, vymax, axmax, aymax, footDimensions, ySep,...
%                 alpha, beta, gamma, delta);
controller = YalmipOptimizerController(T, zCoM, N, Nf, Nr, Nstep,...
                vxmax, vymax, axmax, aymax, footDimensions, ySep,...
                alpha, beta, gamma, delta, xi, dthetaMax);
% controller = YalmipRotationController(T, zCoM, N, Nf, Nr, Nstep,...
%                 vxmax, vymax, axmax, aymax, footDimensions, ySep,...
%                 alpha, beta, gamma, delta, xi, dthetaMax * T);
% controller = FirstFootstepMIQPController(T, zCoM, N, Nstep,...
%                 vxmax, vymax, axmax, aymax, footDimensions, ySep,...
%                 alpha, beta, gamma, delta);

dynamics = LIPMDynamics(T / Ncon, zCoM);

simulation = SimulationRotation(dynamics, controller, simulationParams);

simulation.simulate(Nsim, dxref, dyref, dtheta, disturbances);

plotter = Plotter();

plotter.plotSimulation(simulation.Xh, simulation.Yh,...
    simulation.Zxh, simulation.Zyh, simulation.Xch, simulation.Ych,...
    simulation.Thetach, footDimensions, Ncon);

title('Trajectory', 'FontSize', 14, 'interpreter', 'latex')

% plotter.plotSimulationClearer(simulation.Xh, simulation.Yh,...
%     simulation.Zxh, simulation.Zyh, simulation.Xch, simulation.Ych,...
%     simulation.Thetach, footDimensions, Ncon, disturbances(1).kd);

save('simulation.mat');

times = (T / Ncon) * (1:Nsim);

fontSize = 14;

plotWithDisturbance('Speed', '$m/s$', times, simulation.dXh, simulation.dYh,...
    disturbances);
plotWithDisturbance('Acceleration', '$m/s^2$', times, simulation.d2Xh, simulation.d2Yh,...
    disturbances);
% ylim([-axmax, axmax]);
plotWithDisturbance('Jerk', '$m/s^3$', times, simulation.d3Xh, simulation.d3Yh,...
    disturbances);
plotWithDisturbance('Support foot position', '$m$', times, simulation.Xch, simulation.Ych,...
    disturbances);

figure;
plot(times, simulation.Thetach, 'LineWidth', 2);
hold on;
for p=1:length(disturbances)
    px = [disturbances(p).time, disturbances(p).time];
    py = [min(simulation.Thetach), max(simulation.Thetach)];
     plot(px, py, 'k-.', 'LineWidth', 2);
end
xlabel('Time ($s$)', 'FontSize', fontSize, 'interpreter', 'latex');
ylabel('Support foot rotation ($rad$)', 'FontSize', fontSize, 'interpreter', 'latex');
grid on;
set(gca, 'FontSize', fontSize);
set(gca, 'TickLabelInterpreter','latex')
title('Support Foot Rotation Over Time', 'FontSize', 14, 'interpreter', 'latex')
plotWithDisturbance('Future foot', '$m$', times, simulation.Xf1h, simulation.Yf1h,...
    disturbances);
plotWithDisturbance('Air foot', '$m$', times, simulation.xah, simulation.yah,...
    disturbances);

figure;
hold on;
xlabel('Time ($s$)', 'FontSize', fontSize, 'interpreter', 'latex')
ylabel('Solver Time ($s$)', 'FontSize', fontSize, 'interpreter', 'latex')
set(gca, 'FontSize', fontSize);
set(gca, 'TickLabelInterpreter','latex')
grid on
plot(controller.processingTimes, 'LineWidth', 2);
mean(controller.processingTimes)

% makeSimulationVideo(times, simulation, footDimensions,...
%     disturbances, config);

end

function plotWithDisturbance(plotType, unit, times, valuesX, valuesY,...
    disturbances)
global experimentNumber
fontSize = 14;

figure;
plot(times, valuesX, 'LineWidth', 2);
hold on;
plot(times, valuesY, 'r', 'LineWidth', 2);
for p=1:length(disturbances)
    px = [disturbances(p).time, disturbances(p).time];
    py = [min(min(valuesX), min(valuesY)),...
        max(max(valuesX), max(valuesY))];
    plot(px, py, 'k-.', 'LineWidth', 2);
end
legend(sprintf('x %s', lower(plotType)), sprintf('y %s', lower(plotType)), 'interpreter', 'latex');
xlabel('Time ($s$)', 'FontSize', fontSize, 'interpreter', 'latex');
ylabel(sprintf('%s (%s)', plotType, unit), 'FontSize', fontSize, 'interpreter', 'latex');
grid on;
set(gca, 'FontSize', fontSize);
set(gca, 'TickLabelInterpreter','latex')

if (strcmp(plotType, 'Speed'))
        title('Speeds Over Time', 'FontSize', fontSize, 'interpreter', 'latex')
    end
    if (strcmp(plotType, 'Acceleration'))
        title('Accelerations Over Time', 'FontSize', fontSize, 'interpreter', 'latex')
    end
    if (strcmp(plotType, 'Jerk'))
        title('Jerks Over Time', 'FontSize', fontSize, 'interpreter', 'latex')
    end

end