function runSimulationImitation()


    configurePath();
    loadGurobi();
    yalmip('clear')
    warning('off','all');
    

    clear all; close all; clc
    global experimentNumber
    experimentNumber = '4';

    NeuralNetwork = importKerasNetwork('../2023-03-02 00-55-53/imitationLearningMatlab.h5');
    fid = fopen('../2023-03-02 00-55-53/NN_Info.json');
            raw = fread(fid, inf);
            str = char(raw');
            fclose(fid);
            scaler = jsondecode(str);

    T = 0.06;
    zCoM = 0.24;
    footDimensions = struct;
    footDimensions.length = 0.055;
    footDimensions.width = 0.025;
    vxmax = 0.6;
    vymax = 0.6;
    N = 10;
    Nf = 3;
    Nr = 11;
    Nstep = 5;
    alpha = 10^-6;
    beta = 1;
    gamma = 10^-4;
    ySep = 0.11;
    xc = 0;
    yc = ySep;
    thetac = 0;
    x = [0; 0; 0];
    y = [ySep; 0; 0];
    xa = 0;
    ya = 0;
    dxref = 0.2;
    dyref = 0.0;
    dtheta = 0;
    isLeftSwing = false;
    Ncon = 18;
    Nprev = 5;
    Nsim = N * Ncon * Nprev;
    axmax = 2;
    aymax = 2;
    dthetaMax = 2.0 * (2.0 * pi);


    config.Ncon = Ncon;
    config.title = 'Push from the right while rotating -- x1/6';

    simulationParams.state0 = [x; y];
    simulationParams.xc = xc;
    simulationParams.yc = yc;
    simulationParams.thetac = thetac;
    simulationParams.xa = xa;
    simulationParams.ya = ya;
    simulationParams.thetaa = 0;plot(0.1);grid on
    simulationParams.isLeftSwing = isLeftSwing;

    disturbances(1).kd = 2 * N * Ncon + 1;
    disturbances(1).time = disturbances(1).kd * T / Ncon;
    disturbances(1).xDist = [0.0; -0.0; 0];
    disturbances(1).yDist = [0; -0.0; 0];
    

    delta = 10;
    xi = 10;


    controller = ImitationLearningController(T, N, Nstep, NeuralNetwork, scaler);
    dynamics = LIPMDynamics(T / Ncon, zCoM);

    simulation = SimulationImitation(dynamics, controller, simulationParams);
    simulation.simulate(Nsim, dxref, dyref, dtheta, disturbances);

    plotter = Plotter();

    plotter.plotSimulation(simulation.Xh, simulation.Yh,...
        simulation.Zxh, simulation.Zyh, simulation.Xch, simulation.Ych,...
        simulation.Thetach, footDimensions, Ncon);
    title('Trajectory', 'FontSize', 14, 'interpreter', 'latex')

    save('simulation.mat');

    times = (T / Ncon) * (1:Nsim);

    fontSize = 14;

    plotWithDisturbance('Speed', '$m/s$', times, simulation.dXh, simulation.dYh,...
        disturbances);
    plotWithDisturbance('Acceleration', '$m/s^2$', times, simulation.d2Xh, simulation.d2Yh,...
        disturbances);
    plotWithDisturbance('Jerk', '$m/s^3$', times, simulation.d3Xh, simulation.d3Yh,...
        disturbances);
    plotWithDisturbance('Support foot position', '$m$', times, simulation.Xch, simulation.Ych,...
        disturbances);

    figure;
    plot(times, simulation.Thetach, 'LineWidth', 2);
    hold on;
    for p=1:length(disturbances)
        px = [disturbances(p).time, disturbances(p).time];
        py = [min(simulation.Thetach), max(simulation.Thetach)];
         plot(px, py, 'k-.', 'LineWidth', 2);
    end
    xlabel('Time ($s$)', 'FontSize', fontSize, 'interpreter', 'latex');
    ylabel('Support foot rotation ($rad$)', 'FontSize', fontSize, 'interpreter', 'latex');
    grid on;
    set(gca, 'FontSize', fontSize);
    set(gca, 'TickLabelInterpreter','latex')
    title('Support Foot Rotation Over Time', 'FontSize', 14, 'interpreter', 'latex')
%     axis([0, 3, 0, 2.5])
    plotWithDisturbance('Future foot', '$m$', times, simulation.Xf1h, simulation.Yf1h,...
        disturbances);
    plotWithDisturbance('Air foot', '$m$', times, simulation.xah, simulation.yah,...
        disturbances);


    assignin('base', 'd3X', simulation.controllerHistory.d3X)
    assignin('base', 'd3Xn', simulation.networkHistory.d3X)

    assignin('base', 'd3Y', simulation.controllerHistory.d3Y)
    assignin('base', 'd3Yn', simulation.networkHistory.d3Y)

    assignin('base', 'Xf2', simulation.controllerHistory.Xf2)
    assignin('base', 'Xf2n', simulation.networkHistory.Xf2)

    assignin('base', 'Yf2', simulation.controllerHistory.Yf2)
    assignin('base', 'Yf2n', simulation.networkHistory.Yf2)

    assignin('base', 'Thetaf2', simulation.controllerHistory.Thetaf2)
    assignin('base', 'Thetaf2n', simulation.networkHistory.Thetaf2)

    assignin('base', 'S1', simulation.controllerHistory.S1)
    assignin('base', 'S1n', simulation.networkHistory.S1)

    assignin('base', 'Kprime', simulation.controllerHistory.Kprime)

    assignin('base', 'X', simulation.controllerHistory.X)
    assignin('base', 'dX', simulation.controllerHistory.dX)
    assignin('base', 'd2X', simulation.controllerHistory.d2X)
    assignin('base', 'Y', simulation.controllerHistory.Y)
    assignin('base', 'dY', simulation.controllerHistory.dY)
    assignin('base', 'd2Y', simulation.controllerHistory.d2Y)
    assignin('base', 'Xa', simulation.controllerHistory.Xa)
    assignin('base', 'Ya', simulation.controllerHistory.Ya)
    assignin('base', 'Thetaa', simulation.controllerHistory.Thetaa)
    assignin('base', 'Xc', simulation.controllerHistory.Xc)
    assignin('base', 'Yc', simulation.controllerHistory.Yc)
    assignin('base', 'Thetac', simulation.controllerHistory.Thetac)
    assignin('base', 'Bls', simulation.controllerHistory.Bls)
    assignin('base', 'dXr', simulation.controllerHistory.dXr)
    assignin('base', 'dYr', simulation.controllerHistory.dYr)
    assignin('base', 'dThetar', simulation.controllerHistory.dThetar)



end

function plotWithDisturbance(plotType, unit, times, valuesX, valuesY,...
    disturbances)
    global experimentNumber
    fontSize = 14;

    figure;
    plot(times, valuesX, 'LineWidth', 2);
    hold on;
    plot(times, valuesY, 'r', 'LineWidth', 2);
    for p=1:length(disturbances)
        px = [disturbances(p).time, disturbances(p).time];
        py = [min(min(valuesX), min(valuesY)),...
            max(max(valuesX), max(valuesY))];
        plot(px, py, 'k-.', 'LineWidth', 2);
    end
    legend(sprintf('x %s', lower(plotType)), sprintf('y %s', lower(plotType)), 'interpreter', 'latex');
    xlabel('Time ($s$)', 'FontSize', fontSize, 'interpreter', 'latex');
    ylabel(sprintf('%s (%s)', plotType, unit), 'FontSize', fontSize, 'interpreter', 'latex');
    grid on;
    
    set(gca, 'FontSize', fontSize);
    set(gca, 'TickLabelInterpreter','latex')
    if (strcmp(plotType, 'Speed'))
        title('Speeds Over Time', 'FontSize', fontSize, 'interpreter', 'latex')
    end
    if (strcmp(plotType, 'Acceleration'))
        title('Accelerations Over Time', 'FontSize', fontSize, 'interpreter', 'latex')
    end
    if (strcmp(plotType, 'Jerk'))
        title('Jerks Over Time', 'FontSize', fontSize, 'interpreter', 'latex')
    end
    

end