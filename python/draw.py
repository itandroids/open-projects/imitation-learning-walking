
import matplotlib.pyplot as plt
import pandas as pd

plt.rcParams.update({
    "text.usetex": True,
    "text.latex.preamble": r"\usepackage{amsmath}"
})
plt.rcParams.update ({'font.family': 'serif' })
plt.rcParams.update ({'font.size': 16})


def debug_dataset(X_train, Y_train):
    print("\n", X_train.shape)
    print(X_train.mean(axis=1))
    print(X_train.std(axis=1))
    print(Y_train.shape)
    print(Y_train.mean(axis=1))
    print(Y_train.std(axis=1), "\n")
    print(X_train[0], Y_train[0])

    for i in range(0, X_train.shape[1]):

        x = [row[i] for row in X_train]
        plt.figure()
        plt.ylabel('X'+str(i))
        plt.plot(sorted(x), label='Xi')
        plt.legend()
        plt.show()

def plot_history(history, logdir, dictionary):
    hist = pd.DataFrame(history.history)
    hist['epoch'] = history.epoch

    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel(dictionary['loss_regression'])
    plt.plot(hist['epoch'], hist['Regression_loss'], label='Train Loss', linewidth=2)
    plt.plot(hist['epoch'], hist['val_Regression_loss'], label = 'Val Loss', linewidth=2)
    plt.legend()
    plt.grid()
    plt.savefig(logdir + '/Regression_loss.pdf')

    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel(dictionary['loss_classification'])
    plt.plot(hist['epoch'], hist['Classification_loss'], label='Train Loss', linewidth=2)
    plt.plot(hist['epoch'], hist['val_Classification_loss'], label = 'Val Loss', linewidth=2)
    plt.legend()
    plt.grid()
    plt.savefig(logdir + '/Classification_loss.pdf')

    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.plot(hist['epoch'], hist['loss'], label='Train Loss', linewidth=2)
    plt.plot(hist['epoch'], hist['val_loss'], label = 'Val Loss', linewidth=2)
    plt.legend()
    plt.grid()
    plt.savefig(logdir + '/Total_loss.pdf')