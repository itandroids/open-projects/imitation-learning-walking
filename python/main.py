from neural_network import NeuralNetwork
from train_utils import *
from argparser import *

def main():
    args = get_parser().parse_args()
    validate_inputs(args)
    argparse_dict = vars(args)

    X_train, Y_train = dataset(args.version, args.input_scaler, args.output_scaler)
    X_train, X_test, Y_train, Y_test = divide_dataset(X_train, Y_train, args.test_split)

    nn = NeuralNetwork(args, X_train, Y_train)

    history = nn.train(X_train, Y_train, X_test, Y_test)
    scores = nn.evaluate_model(X_train, Y_train)

    log(nn, history, X_train, [nn.model.metrics_names, scores], argparse_dict)

if __name__ == '__main__':
	main()

