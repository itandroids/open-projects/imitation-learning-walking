function plotFirstFootstepOptimizationTest(testName)

timeFile = load(['time_', testName, '.txt']);
footstepFile = load(['footstep_', testName, '.txt']);

d3X = timeFile(:, 1);
d3Y = timeFile(:, 2);
Xfaux = timeFile(:, 3);
Yfaux = timeFile(:, 4);

Xfb = footstepFile(:, 1);
Yfb = footstepFile(:, 2);
theta = zeros(size(Xfaux, 1), 1);

Xfb = Xfb(1:2, 1);
Yfb = Yfb(1:2, 1);

T = 0.1;
zCOM = 0.2;
N = 10;
prediction = Prediction(LIPMDynamics(T, zCOM), N);
x = [0; 0; 0];
y = [0; 0; 0];
footDimensions.length = 0.06;
footDimensions.width = 0.03;

PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, theta,...
                prediction, x, y, T, N, footDimensions);
            
end