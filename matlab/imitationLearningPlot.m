function imitationLearningPlot()

    clear all; close all; clc
    evalin('base','clear')
    
    global dataset;
    
    dataset = load('trajectories_dataset.mat');
    
    
    variables = fields(dataset);
    for i=1:1:size(variables)
        dataset.(variables{i}) = reshape(dataset.(variables{i})', [size(dataset.(variables{i}),1)*size(dataset.(variables{i}),2),1]);
    end
    
    indices = find(dataset.feasible<0.5);
    
    for i=1:1:size(variables)
        dataset.(variables{i})(indices) = [];
    end
    
    adjust_reference()
    
    for i=1:1:size(variables)
        %dataset.(variables{i}) = normalize(dataset.(variables{i}));
        %dataset.(variables{i})(isnan(dataset.(variables{i}))) = 0;
        assignin('base', variables{i}, dataset.(variables{i}))
    end
    
    [X_train, Y_train] = build_dataset_v1();
    normalize_data('scaler.json')
    
    model = importKerasNetwork('imitationLearningMatlab.h5');
    assignin('base', 'model', model)
    
    plot(X_train(:,11))
    figure(2)
    plot(Y_train(:,6))
    disp(model.predict(X_train(1204,:)))
    
    
    
    
    
    
    
    
    function [x,y] = global_to_relative(x,y, x_origin, y_origin, theta_origin)        
        x = x - x_origin;
        y = y - y_origin;
        [x,y] = rotate(x,y, -theta_origin);      
    end


    function [x,y] = rotate(x,y,theta)     
        x_old = x;
        x = x.* cos(theta)     - y.*sin(theta);
        y = x_old.* sin(theta) + y.*cos(theta);        
    end


    function result = invert_if_left_support(vector,bls)
        result = bls.*vector + (bls-1).*vector;
    end


    function adjust_reference()
        [dataset.X, dataset.Y] = global_to_relative(dataset.X, dataset.Y, dataset.Xc, dataset.Yc, dataset.Thetac);
        [dataset.dX, dataset.dY] = rotate(dataset.dX, dataset.dY, -dataset.Thetac);
        [dataset.d2X, dataset.d2Y] = rotate(dataset.d2X, dataset.d2Y, -dataset.Thetac);

        [dataset.Xa, dataset.Ya] = global_to_relative(dataset.Xa, dataset.Ya, dataset.Xc, dataset.Yc, dataset.Thetac);
        dataset.Thetaa = dataset.Thetaa - dataset.Thetac;

        [dataset.dXr, dataset.dYr] = rotate(dataset.dXr, dataset.dYr, -dataset.Thetac);

        [dataset.d3X, dataset.d3Y] = rotate(dataset.d3X, dataset.d3Y, -dataset.Thetac);
        [dataset.Xf2, dataset.Yf2] = global_to_relative(dataset.Xf2, dataset.Yf2, dataset.Xc, dataset.Yc, dataset.Thetac);
        dataset.Thetaf2 = dataset.Thetaf2 - dataset.Thetac;
        
        dataset.Y = invert_if_left_support(dataset.Y,dataset.Bls);
        dataset.dY = invert_if_left_support(dataset.dY,dataset.Bls);
        dataset.d2Y = invert_if_left_support(dataset.d2Y,dataset.Bls);
        dataset.Ya = invert_if_left_support(dataset.Ya,dataset.Bls);
        dataset.Thetaa = invert_if_left_support(dataset.Thetaa,dataset.Bls);
        dataset.dYr = invert_if_left_support(dataset.dYr,dataset.Bls);
        dataset.d3Y = invert_if_left_support(dataset.d3Y,dataset.Bls);
        dataset.Yf2 = invert_if_left_support(dataset.Yf2,dataset.Bls);
        dataset.Thetaf2 = invert_if_left_support(dataset.Thetaf2,dataset.Bls);
    end

    function [X, Y] = build_dataset_v1()
        X = [dataset.X, dataset.dX, dataset.d2X, dataset.Y, dataset.dY, dataset.d2Y, ...
                   dataset.Xa, dataset.Ya, dataset.Thetaa, dataset.dXr, dataset.dYr, dataset.dThetar, dataset.Kprime];
        Y = [dataset.d3X, dataset.d3Y, dataset.Xf2, dataset.Yf2, dataset.Thetaf2, dataset.S1, dataset.feasible];
        assignin('base', 'X_train', X)
        assignin('base', 'Y_train', Y)
        
    end


    function  normalize_data(file_name)
        fid = fopen(file_name);
        raw = fread(fid, inf);
        str = char(raw');
        fclose(fid);
        scaler = jsondecode(str);
        assignin('base', 'scaler', scaler)  
        
        if scaler.input_scaler == "standard"
            X_train = (X_train - scaler.input_mean') ./ scaler.input_std';
            X_train(isnan(X_train)) = 0;
        else 
            X_train = (X_train - scaler.input_min') ./ (scaler.input_max' - scaler.input_min');
            X_train(isnan(X_train)) = 0;
        end
        
        if scaler.output_scaler == "standard"
            Y_train = (Y_train - scaler.output_mean') ./ scaler.output_std';
            Y_train(isnan(Y_train)) = 0;
        else 
            Y_train = (Y_train - scaler.output_min') ./ (scaler.output_max' - scaler.output_min');
            Y_train(isnan(Y_train)) = 0;
        end
        
        assignin('base', 'X_train', X_train)
        assignin('base', 'Y_train', Y_train)
    end
        
    
end



