from random import choices
from config import Constants
from neural_network import NeuralNetwork
from train_utils import *
from argparser import *
import os

import tensorflow as tf
from keras.callbacks import ModelCheckpoint
from keras import backend as K
import numpy as np

import pandas as pd

import optuna

import plotly 

plotly.io.kaleido.scope.mathjax= None

import warnings
warnings.filterwarnings("ignore", message=r"KerasPruningCallback", category=FutureWarning)
warnings.simplefilter("ignore", UserWarning)


def main():
    args = argparse.Namespace()
    args.version =  'v2'
    args.input_scaler =  'standard'
    args.output_scaler =  'standard'
    args.loss_regression =  'mae'
    args.loss_classification =  'mae'
    args.final_activation_regression =  'linear'
    args.final_activation_classification =  'sigmoid'
    args.activation =  'relu'
    args.lr = 0.0001
    args.nb_layers =  1
    args.nb_neurons =  [1]
    args.epochs = 150
    args.test_split = 10
    args.alpha_loss = 0.15
    args.dropout_rate = 0.0
    args.batch_size = 128
    
    X_train, Y_train = dataset(args.version, args.input_scaler, args.output_scaler)
    X_train, X_test, Y_train, Y_test = divide_dataset(X_train, Y_train, args.test_split)

    def objective(trials):

        args.nb_layers = int(trials.suggest_discrete_uniform(name = 'Number of Layers', low = 1, high = 10, q = 1))
        args.nb_neurons = [int(trials.suggest_discrete_uniform(name = 'Number of Neurons', low = 50, high = 2000, q = 50))] * args.nb_layers
        args.lr = trials.suggest_float("Initial Learning Rate", 1e-6, 1e-3, log=True)
        args.final_activation_regression = trials.suggest_categorical(name = 'Regression Activation', choices= ['linear', 'relu', 'sigmoid', 'tanh', 'elu', 'selu', 'exponential'])
        args.final_activation_classification = trials.suggest_categorical(name = 'Classification Activation ', choices= ['linear', 'relu', 'sigmoid', 'tanh', 'elu', 'selu', 'exponential'])
        args.activation = trials.suggest_categorical(name = 'Hidden Activation', choices= ['linear', 'relu', 'sigmoid', 'tanh', 'elu', 'selu', 'exponential'])

        pruning = optuna.integration.KerasPruningCallback(trials, monitor = 'val_loss', interval=1)
        nn = NeuralNetwork(args, X_train, Y_train)    
        history = nn.train(X_train, Y_train, X_test, Y_test, pruning)
        hist = pd.DataFrame(history.history)
        hist['val_loss']

        return np.min(hist['val_loss'])

    study = optuna.create_study(direction = 'minimize', pruner = optuna.pruners.HyperbandPruner(), sampler = optuna.samplers.TPESampler(multivariate=True), 
                                study_name='Hyperparameters Optimization')
    study.optimize(objective, n_trials=100)

    fig1 = optuna.visualization.plot_optimization_history(study)
    fig1.write_image('optimization_history.pdf', engine="orca", format="pdf")
    fig2 = optuna.visualization.plot_slice(study)
    fig2.write_image('plot_slice2.pdf', engine="orca", format="pdf")
    fig3 = optuna.visualization.plot_contour(study)
    fig3.write_image('plot_contour.pdf', engine="orca", format="pdf")
    fig4 = optuna.visualization.plot_edf(study)
    fig4.write_image('plot_edf.pdf', engine="orca", format="pdf")
    fig5 = optuna.visualization.plot_param_importances(study)
    fig5.write_image('plot_param_importances.pdf', engine="orca", format="pdf")
    fig6 = optuna.visualization.plot_parallel_coordinate(study)
    fig6.write_image('plot_parallel_coordinate.pdf', engine="orca", format="pdf")
    fig7 = optuna.visualization.plot_intermediate_values(study)
    fig7.write_image('plot_intermediate_values.pdf', engine="orca", format="pdf")

if __name__ == '__main__':
	main()