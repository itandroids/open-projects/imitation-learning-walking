# Imitation Learning Walking

## Overview

This is the public repository for the Master's project of Vítor Giudice Batista de Araujo Porto

## Table of Contents

The repository includes the following folders:

- `datasets`: contains the datasets used for training and testing
- `python`: contains Python scripts for running the imitation learning training and testing
- `matlab`: contains Matlab scripts for generating the datasets and testing the model with LIPM simulations
- `YALMIP Master`: contains configurations for YALMIP for the MPC

## Requirements

To get the requirements to run the python codes, simply install the provided conda environment:

```bash
conda env create -f environment.yml
```

## Usage

### Python
 - To train the network, simply run the [training_script.sh](https://gitlab.com/giudice.vitor/mestrado/-/blob/main/python/training_script.sh) to use the same parameters as in the original paper.
 - To test the results, run the [teste.py](https://gitlab.com/giudice.vitor/mestrado/-/blob/main/python/teste.py) script

### MATLAB
 - To test the model with the LIPM simulation, simply run the [runSimulationImitation.m](https://gitlab.com/giudice.vitor/mestrado/-/blob/main/matlab/runSimulationImitation.m) script
 - To generate new trajectories, run the [generateImitationDatasetTrajectories.m](https://gitlab.com/giudice.vitor/mestrado/-/blob/main/matlab/generateImitationDatasetTrajectories.m) script
## References

- [Link to paper]