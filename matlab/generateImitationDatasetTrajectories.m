function generateImitationDatasetTrajectories()

configurePath();
loadGurobi();
yalmip('clear')
warning('off','all');
    
% Don't judge me
warning('off','all');

% Robot parameters
controllerParams.T = 0.06;
controllerParams.zCoM = 0.24;
controllerParams.ySep = 0.11;
controllerParams.footDimensions = struct;
% controllerParams.footDimensions.length = 0.06;
% controllerParams.footDimensions.width = 0.03;
controllerParams.footDimensions.length = 0.055;
controllerParams.footDimensions.width = 0.025;
controllerParams.vxmax = 0.6;
controllerParams.vymax = 0.4;
controllerParams.dthetaMax = 4*pi;


controllerParams.axmax = 2;
controllerParams.aymax = 2;

% Horizon parameters
controllerParams.N = 10;
controllerParams.Nstep = 5;
controllerParams.Nf = 3;
controllerParams.Nr = 11;

% Control weights
controllerParams.alpha = 10^-6;
controllerParams.beta = 1;
controllerParams.gamma = 10^-4;
controllerParams.delta = 10;
controllerParams.xi = 10;

% Simulation params
simulationParams.Ncon = 18;
simulationParams.Nprev = 5;
simulationParams.Nsim = controllerParams.N * simulationParams.Ncon * simulationParams.Nprev;
numTimestepsController = controllerParams.N * simulationParams.Nprev;

numTrajectories = 3000;

Kdin = zeros(numTrajectories, numTimestepsController);
X = zeros(numTrajectories, numTimestepsController);
dX = zeros(numTrajectories, numTimestepsController);
d2X = zeros(numTrajectories, numTimestepsController);
Y = zeros(numTrajectories, numTimestepsController);
dY = zeros(numTrajectories, numTimestepsController);
d2Y = zeros(numTrajectories, numTimestepsController);
Xa = zeros(numTrajectories, numTimestepsController);
Ya = zeros(numTrajectories, numTimestepsController);
Thetaa = zeros(numTrajectories, numTimestepsController);
dXr = zeros(numTrajectories, numTimestepsController);
dYr = zeros(numTrajectories, numTimestepsController);
dThetar = zeros(numTrajectories, numTimestepsController);
Xc = zeros(numTrajectories, numTimestepsController);
Yc = zeros(numTrajectories, numTimestepsController);
Thetac = zeros(numTrajectories, numTimestepsController);
Kprime = zeros(numTrajectories, numTimestepsController); % 0, 1, 2, 3, 4
Bls = zeros(numTrajectories, numTimestepsController); % 0, 1

Kdout = zeros(numTrajectories, numTimestepsController);
d3X = zeros(numTrajectories, numTimestepsController);
d3Y = zeros(numTrajectories, numTimestepsController);
Xf2 = zeros(numTrajectories, numTimestepsController);
Yf2 = zeros(numTrajectories, numTimestepsController);
Thetaf2 = zeros(numTrajectories, numTimestepsController);
S1 = zeros(numTrajectories, numTimestepsController); % 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
feasible = zeros(numTrajectories, numTimestepsController); % 0, 1

for t=1:numTrajectories
    tic
    fprintf('Trajectory=%d/%d\n', t, numTrajectories);
    bls = randi([0 1]);
    bls = 0;
    if (bls)
        initialy = -controllerParams.ySep;
    else
        initialy = controllerParams.ySep;
    end
    
        
    
    simulationParams.isLeftSwing = bls;
    direction = randi([0,1]);
%     simulationParams.dxref = unifrnd(-controllerParams.vxmax, controllerParams.vxmax)* randi([0,1]);
%     simulationParams.dyref = unifrnd(-controllerParams.vymax, controllerParams.vymax)* randi([0,1]);
%     simulationParams.dtheta = unifrnd(-controllerParams.dthetaMax, controllerParams.dthetaMax)* randi([0,1]);

    simulationParams.dxref = unifrnd(-0.25, 0.25)* randi([0,1]);
    simulationParams.dyref = unifrnd(-0.25, 0.25)* randi([0,1]);
    simulationParams.dtheta = unifrnd(-pi/2, pi/2)* randi([0,1]);
%     simulationParams.dxref = 0;
%     if (direction == 0)
%         simulationParams.dyref = unifrnd(-0.25, 0.25);
%         simulationParams.dtheta = 0;
%     else
%         simulationParams.dyref = 0;
%         simulationParams.dtheta = unifrnd(-pi/2, pi/2);
%     end
%         simulationParams.x = [0;...
%                             0;...
%                             0];
%     simulationParams.y = [initialy;...
%                             0;...
%                             0];                    
%     simulationParams.state0 = [simulationParams.x; simulationParams.y];
        simulationParams.x = [0;...
                            0;...
                            0];
    simulationParams.y = [initialy;...
                            0;...
                            0];                    
    simulationParams.state0 = [simulationParams.x; simulationParams.y];

%     simulationParams.xa = 0;
%     simulationParams.ya = 0;


    simulationParams.xa = 0;
    simulationParams.ya = 0;
    simulationParams.thetaa = 0.0;
    
    
    
%     simulationParams.x = [unifrnd(-0.02, 0.02);...
%                             unifrnd(-0.05, 0.05);...
%                             unifrnd(-controllerParams.axmax, controllerParams.axmax)];
%     simulationParams.y = [unifrnd(-0.02, 0.02);...
%                             unifrnd(-0.05, 0.05);...
%                             unifrnd(-controllerParams.aymax, controllerParams.aymax)];                    
%     simulationParams.state0 = [simulationParams.x; simulationParams.y];
%     simulationParams.xa = unifrnd(-0.02, 0.02);
%     simulationParams.ya = (-1)^(bls + 1) * (0.02 + controllerParams.ySep) + unifrnd(-0.02, 0.02);
%     simulationParams.thetaa = 0.0;
    
    simulationParams.xc = 0.0;
    simulationParams.yc = controllerParams.ySep;
    simulationParams.thetac = 0.0;



%     simulationParams.isLeftSwing = false;
%     simulationParams.dxref = -0.5;
%     simulationParams.dyref = 0;
%     simulationParams.dtheta = 0;
%     simulationParams.x = [0;...
%                             0;...
%                             0];
%     simulationParams.y = [controllerParams.ySep;...
%                             0;...
%                             0];                    
%     simulationParams.state0 = [simulationParams.x; simulationParams.y];
%     simulationParams.xa = 0;
%     simulationParams.ya = 0;
%     simulationParams.thetaa = 0.0;
%     
%     simulationParams.xc = 0.0;
%     simulationParams.yc = controllerParams.ySep;
%     simulationParams.thetac = 0.0;

%     simulationParams.isLeftSwing = false;
%     simulationParams.dxref = unifrnd(-0.3, 0.3);
%     simulationParams.dyref = unifrnd(-0.3, 0.3);
%     simulationParams.dtheta = unifrnd(1, 1);
%     simulationParams.x = [0;...
%                             0;...
%                             0];
%     simulationParams.y = [controllerParams.ySep;...
%                             0;...
%                             0];                    
%     simulationParams.state0 = [simulationParams.x; simulationParams.y];
%     simulationParams.xa = 0;
%     simulationParams.ya = 0;
%     simulationParams.thetaa = 0.0;
    
    
    simulation = generateTrajectory(controllerParams, simulationParams);
    
    Kdin(t, :) = simulation.controllerHistory.Kdin';
    X(t, :) = simulation.controllerHistory.X';
    dX(t, :) = simulation.controllerHistory.dX';
    d2X(t, :) = simulation.controllerHistory.d2X';
    Y(t, :) = simulation.controllerHistory.Y';
    dY(t, :) = simulation.controllerHistory.dY';
    d2Y(t, :) = simulation.controllerHistory.d2Y';
    Xa(t, :) = simulation.controllerHistory.Xa';
    Ya(t, :) = simulation.controllerHistory.Ya';
    Thetaa(t, :) = simulation.controllerHistory.Thetaa';
    dXr(t, :) = simulation.controllerHistory.dXr';
    dYr(t, :) = simulation.controllerHistory.dYr';
    dThetar(t, :) = simulation.controllerHistory.dThetar';
    Xc(t, :) = simulation.controllerHistory.Xc';
    Yc(t, :) = simulation.controllerHistory.Yc';
    Thetac(t, :) = simulation.controllerHistory.Thetac';
    Kprime(t, :) = simulation.controllerHistory.Kprime'; % 0, 1, 2, 3, 4
    Bls(t, :) = simulation.controllerHistory.Bls'; % 0, 1
    
    Kdout(t, :) = simulation.controllerHistory.Kdout';
    d3X(t, :) = simulation.controllerHistory.d3X';
    d3Y(t, :) = simulation.controllerHistory.d3Y';
    Xf2(t, :) = simulation.controllerHistory.Xf2';
    Yf2(t, :) = simulation.controllerHistory.Yf2';
    Thetaf2(t, :) = simulation.controllerHistory.Thetaf2';
    S1(t, :) = simulation.controllerHistory.S1'; % 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    feasible(t, :) = simulation.controllerHistory.feasible'; % 0, 1
    toc
end

save('luznofimdotunel.mat',...
    'Kdin',...
    'X',...
    'dX',...
    'd2X',...
    'Y',...
    'dY',...
    'd2Y',...
    'Xa',...
    'Ya',...
    'Thetaa',...
    'dXr',...
    'dYr',...
    'dThetar',...
    'Xc',...
    'Yc',...
    'Thetac',...
    'Kprime',...
    'Bls',...
    'Kdout',...
    'd3X',...
    'd3Y',...
    'Xf2',...
    'Yf2',...
    'Thetaf2',...
    'S1',...
    'feasible');

end

function trajectory = generateTrajectory(controllerParams, simulationParams)

yalmip('clear')

N = controllerParams.N;
T = controllerParams.T;

Ncon = simulationParams.Ncon;
Nprev = simulationParams.Nprev;
Nsim = N * Ncon * Nprev;

% disturbanceIndices = N:(N * Nprev);
disturbanceIndices = 1;
disturbances(length(disturbanceIndices)) = struct;
dist = randi([0,1]);
if (dist == 0)
    for d=1:length(disturbanceIndices)
%         disturbances(d).kd = 2 * d * Ncon + 1;
        disturbances(d).kd = randi([100,500]);
        disturbances(d).time = disturbances(d).kd * T / Ncon;
        disturbances(d).xDist = [0.0; unifrnd(-0.3, 0.3); 0.0];
        disturbances(d).yDist = [0.0; unifrnd(-0.25, 0.25); 0.0];
    end
else
    for d=1:length(disturbanceIndices)
        disturbances(d).kd = 2 * d * Ncon + 1;
        disturbances(d).time = disturbances(d).kd * T / Ncon;
        disturbances(d).xDist = [0.0; 0; 0.0];
        disturbances(d).yDist = [0.0; 0; 0.0];
    end
end
% 
%     for d=1:length(disturbanceIndices)
%         disturbances(d).kd = 2 * d * Ncon + 1;
%         disturbances(d).time = disturbances(d).kd * T / Ncon;
%         disturbances(d).xDist = [0.0; 0; 0.0];
%         disturbances(d).yDist = [0.0; 0; 0.0];
%     end

controller = YalmipOptimizerController(...
                    controllerParams.T,...
                    controllerParams.zCoM,...
                    controllerParams.N,...
                    controllerParams.Nf,...
                    controllerParams.Nr,...
                    controllerParams.Nstep,...
                    controllerParams.vxmax,...
                    controllerParams.vymax,...
                    controllerParams.axmax,...
                    controllerParams.aymax,...
                    controllerParams.footDimensions,...
                    controllerParams.ySep,...
                    controllerParams.alpha,...
                    controllerParams.beta,...
                    controllerParams.gamma,...
                    controllerParams.delta,...
                    controllerParams.xi,...
                    controllerParams.dthetaMax);

dynamics = LIPMDynamics(T / Ncon, controllerParams.zCoM);

simulation = SimulationRotation(dynamics, controller, simulationParams);

simulation.simulate(Nsim, simulationParams.dxref, simulationParams.dyref, simulationParams.dtheta, disturbances);

% plotter = Plotter();
% 
% plotter.plotSimulation(simulation.Xh, simulation.Yh,...
%     simulation.Zxh, simulation.Zyh, simulation.Xch, simulation.Ych,...
%     simulation.Thetach, controllerParams.footDimensions, Ncon);

trajectory = simulation;

end