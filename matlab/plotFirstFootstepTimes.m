function plotFirstFootstepTimes()

setupTime = load('sequence_setup_time.txt');
solverTime = load('sequence_solver_time.txt');

meanSetupTime = mean(setupTime);
stdSetupTime = std(setupTime);

meanSolverTime = mean(solverTime);
stdSolverTime = std(solverTime);

figure;
hold on;
errorbar(meanSetupTime, 2 * stdSetupTime, 'LineWidth', 2);
xlabel('Duration of the first footstep (timesteps)', 'FontSize', 14);
ylabel('Setup time (s)', 'FontSize', 14);
set(gca, 'FontSize', 14);
grid on;
xlim([1, 10]);
% title ('First Footstep - Setup Time')

figure;
hold on;
errorbar(meanSolverTime, 2 * stdSolverTime, 'LineWidth', 2);
xlabel('Duration of the first footstep (timesteps)', 'FontSize', 14);
ylabel('Solver time (s)', 'FontSize', 14);
set(gca, 'FontSize', 14);
grid on;
xlim([1, 10]);
% title ('First Footstep - Solver Time')

figure;
hold on;
sumSetupTime = sum(setupTime, 2);
sumSolverTime = sum(solverTime, 2);
plot(sumSetupTime + sumSolverTime, 'LineWidth', 2);
plot((mean(sumSetupTime) + mean(sumSolverTime)) * ones(size(solverTime, 1), 1), 'r--', 'LineWidth', 2);
xlabel('Iteration (-)', 'FontSize', 14);
ylabel('Time (s)', 'FontSize', 14);
set(gca, 'FontSize', 14);
grid on;
% title('Total Time for Sequence of QPs');

end