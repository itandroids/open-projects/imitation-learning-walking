import scipy.io as sio
import numpy as np
from utils import global_to_relative, rotate, invert_if_left_support
from sklearn.preprocessing import OneHotEncoder

trajectory_dataset = True
# data = sio.loadmat('set2023.mat', squeeze_me=True)
data = sio.loadmat('../datasets/dataset.mat', squeeze_me=True)

X = data['X']
dX = data['dX']
d2X = data['d2X']
Y = data['Y']
dY = data['dY']
d2Y = data['d2Y']
Xa = data['Xa']
Ya = data['Ya']
Thetaa = data['Thetaa']
dXr = data['dXr']
dYr = data['dYr']
dThetar = data['dThetar']
Xc = data['Xc']
Yc = data['Yc']
Thetac = data['Thetac']
Kprime = data['Kprime']
Bls = data['Bls']

d3X = data['d3X']
d3Y = data['d3Y']
Xf2 = data['Xf2']
Yf2 = data['Yf2']
Thetaf2 = data['Thetaf2']
S1 = data['S1']
feasible = data['feasible']

Bds = np.zeros((X.shape[0], X.shape[1]))

for i in range (0,feasible.shape[0]):
    for j in range(0,feasible.shape[1]-1):
        if S1[i][j] == 1:
            Bds[i][j+1] = 1

if trajectory_dataset:

    num_elements = feasible.shape[0] * feasible.shape[1]

    X = X.reshape(num_elements)
    dX = dX.reshape(num_elements)
    d2X = d2X.reshape(num_elements)
    Y = Y.reshape(num_elements)
    dY = dY.reshape(num_elements)
    d2Y = d2Y.reshape(num_elements)
    Xa = Xa.reshape(num_elements)
    Ya = Ya.reshape(num_elements)
    Thetaa = Thetaa.reshape(num_elements)
    dXr = dXr.reshape(num_elements)
    dYr = dYr.reshape(num_elements)
    dThetar = dThetar.reshape(num_elements)
    Xc = Xc.reshape(num_elements)
    Yc = Yc.reshape(num_elements)
    Thetac = Thetac.reshape(num_elements)
    Kprime = Kprime.reshape(num_elements)
    Bls = Bls.reshape(num_elements)

    d3X = d3X.reshape(num_elements)
    d3Y = d3Y.reshape(num_elements)
    Xf2 = Xf2.reshape(num_elements)
    Yf2 = Yf2.reshape(num_elements)
    Thetaf2 = Thetaf2.reshape(num_elements)
    S1 = S1.reshape(num_elements)
    feasible = feasible.reshape(num_elements)
    Bds = Bds.reshape(num_elements)


    percentage_bad = 0.0
    indices = np.where(feasible >= 0)
    indices2 = np.where(feasible < 0)
    indices_bad = np.where(feasible < 0)
    num_bad = int(np.floor(np.size(indices_bad) * percentage_bad))
    indices_bad = indices_bad[0][:num_bad]


    X = np.append(X[indices], X[indices_bad])
    dX = np.append(dX[indices], dX[indices_bad])
    d2X = np.append(d2X[indices], d2X[indices_bad])
    Y = np.append(Y[indices], Y[indices_bad])
    dY = np.append(dY[indices], dY[indices_bad])
    d2Y = np.append(d2Y[indices], d2Y[indices_bad])
    Xa = np.append(Xa[indices], Xa[indices_bad])
    Ya = np.append(Ya[indices], Ya[indices_bad])
    Thetaa = np.append(Thetaa[indices], Thetaa[indices_bad])
    dXr = np.append(dXr[indices], dXr[indices_bad])
    dYr = np.append(dYr[indices], dYr[indices_bad])
    dThetar = np.append(dThetar[indices], dThetar[indices_bad])
    Xc = np.append(Xc[indices], Xc[indices_bad])
    Yc = np.append(Yc[indices], Yc[indices_bad])
    Thetac = np.append(Thetac[indices], Thetac[indices_bad])
    Kprime = np.append(Kprime[indices], Kprime[indices_bad])
    Bls = np.append(Bls[indices], Bls[indices_bad])

    d3X = np.append(d3X[indices], d3X[indices_bad])
    d3Y = np.append(d3Y[indices], d3Y[indices_bad])
    Xf2 = np.append(Xf2[indices], Xf2[indices_bad])
    Yf2 = np.append(Yf2[indices], Yf2[indices_bad])
    Thetaf2 = np.append(Thetaf2[indices], Thetaf2[indices_bad])
    S1 = np.append(S1[indices], S1[indices_bad])
    feasible = np.append(feasible[indices], feasible[indices_bad])
    Bds = np.append(Bds[indices], Bds[indices_bad])


X, Y = global_to_relative(X, Y, Xc, Yc, Thetac)
dX, dY = rotate(dX, dY, -Thetac)
d2X, d2Y = rotate(d2X, d2Y, -Thetac)
Xa, Ya = global_to_relative(Xa, Ya, Xc, Yc, Thetac)
Thetaa = Thetaa - Thetac
dXr, dYr = rotate(dXr, dYr, -Thetac)

d3X, d3Y = rotate(d3X, d3Y, -Thetac)
Xf2, Yf2 = global_to_relative(Xf2, Yf2, Xc, Yc, Thetac)
Thetaf2 = Thetaf2 - Thetac

Y = invert_if_left_support(Y, Bls)
dY = invert_if_left_support(dY, Bls)
d2Y = invert_if_left_support(d2Y, Bls)
Ya = invert_if_left_support(Ya, Bls)
Thetaa = invert_if_left_support(Thetaa, Bls)
dYr = invert_if_left_support(dYr, Bls)
dThetar = invert_if_left_support(dThetar, Bls)
d3Y = invert_if_left_support(d3Y, Bls)
Yf2 = invert_if_left_support(Yf2, Bls)
Thetaf2 = invert_if_left_support(Thetaf2, Bls)

Xa_next = Xa.copy()
Ya_next = Xa.copy()
Thetaa_next = Xa.copy()

for i in range(0, np.size(Xa)):
    if S1 [i] != 0:
        Xa_next[i] = Xa[i] + (Xf2[i] - Xa[i]) / S1[i]
        Ya_next[i] = Ya[i] + (Yf2[i] - Ya[i]) / S1[i]
        Thetaa_next[i] = Thetaa[i] + (Thetaf2[i] - Thetaa[i]) / S1[i]
    else:
        #? what to do in this situation
        Xa_next[i] = 0
        Ya_next[i] = 0
        Thetaa_next[i] = 0
touch = (S1 == 1)

#! Bds manipulations

# indices2 = np.where(Bds == 1)
# Xa_next[indices2] = Xa[indices2] 

#! Bds manipulations


encoder = OneHotEncoder(sparse=False)
Kprime_onehot = encoder.fit_transform(np.reshape(Kprime, [-1,1]))

encoder = OneHotEncoder(sparse=False)
S1_onehot = encoder.fit_transform(np.reshape(S1, [-1,1]))


def build_dataset_v1():
    Xtrain, Ytrain = [], []
    for i in range(len(X)):
        Xtrain.append([ X[i], dX[i], d2X[i], Y[i], dY[i], d2Y[i], Xa[i], Ya[i], Thetaa[i], dXr[i], dYr[i], dThetar[i], Kprime[i]])
        Ytrain.append([ d3X[i], d3Y[i], Xf2[i], Yf2[i], Thetaf2[i], S1[i]])
    
    return Xtrain, Ytrain
        
def build_dataset_v2():
    Xtrain, Ytrain = [], []
    for i in range(len(X)):
        Xtrain.append([ X[i], dX[i], d2X[i], Y[i], dY[i], d2Y[i], Xa[i], Ya[i], Thetaa[i], dXr[i], dYr[i], dThetar[i], Kprime[i]]) # Bds[i]
        Ytrain.append([ d3X[i], d3Y[i], Xa_next[i], Ya_next[i], Thetaa_next[i], touch[i]]) #feasible[i]
    
    return Xtrain, Ytrain

def build_dataset_v1_onehotencoding():
    Xtrain, Ytrain = [], []
    for i in range(len(X)):
        Xtrain.append([ X[i], dX[i], d2X[i], Y[i], dY[i], d2Y[i], Xa[i], Ya[i], Thetaa[i], dXr[i], dYr[i], dThetar[i], Kprime[i]])
        Ytrain.append([ d3X[i], d3Y[i], Xf2[i], Yf2[i], Thetaf2[i]] )
        Ytrain[i] = np.hstack((Ytrain[i], S1_onehot[i])) #feasible[i]

    return Xtrain, Ytrain
        
def build_dataset_v2_onehotencoding():
    Xtrain, Ytrain = [], []
    for i in range(len(X)):
        Xtrain.append([ X[i], dX[i], d2X[i], Y[i], dY[i], d2Y[i], Xa[i], Ya[i], Thetaa[i], dXr[i], dYr[i], dThetar[i], Kprime[i]])
        Ytrain.append([ d3X[i], d3Y[i], Xa_next[i], Ya_next[i], Thetaa_next[i], touch[i]]) #feasible[i]
    
    return Xtrain, Ytrain
    