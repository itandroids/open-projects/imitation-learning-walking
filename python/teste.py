import scipy.io as sio
import numpy as np
from utils import global_to_relative, rotate, invert_if_left_support
from sklearn.preprocessing import OneHotEncoder
import matplotlib.pyplot as plt
import json
from keras.models import load_model

plt.rcParams.update({
    "text.usetex": True,
    "text.latex.preamble": r"\usepackage{amsmath}"
})
plt.rcParams.update ({'font.family': 'serif' })
plt.rcParams.update ({'font.size': 16})

trajectory_dataset = True
data = sio.loadmat('../datasets/test_trajectories.mat', squeeze_me=True)

X = data['X']
dX = data['dX']
d2X = data['d2X']
Y = data['Y']
dY = data['dY']
d2Y = data['d2Y']
Xa = data['Xa']
Ya = data['Ya']
Thetaa = data['Thetaa']
dXr = data['dXr']
dYr = data['dYr']
dThetar = data['dThetar']
Xc = data['Xc']
Yc = data['Yc']
Thetac = data['Thetac']
Kprime = data['Kprime']
Bls = data['Bls']

d3X = data['d3X']
d3Y = data['d3Y']
Xf2 = data['Xf2']
Yf2 = data['Yf2']
Thetaf2 = data['Thetaf2']
S1 = data['S1']
feasible = data['feasible']

print(dXr)
print(20*'--')
print(dYr)
print(20*'--')
print(dThetar)

Bds = np.zeros((X.shape[0], X.shape[1]))

for i in range (0,feasible.shape[0]):
    for j in range(0,feasible.shape[1]-1):
        if S1[i][j] == 1:
            Bds[i][j+1] = 1

if trajectory_dataset:

    num_elements = feasible.shape[0] * feasible.shape[1]

    X = X.reshape(num_elements)
    dX = dX.reshape(num_elements)
    d2X = d2X.reshape(num_elements)
    Y = Y.reshape(num_elements)
    dY = dY.reshape(num_elements)
    d2Y = d2Y.reshape(num_elements)
    Xa = Xa.reshape(num_elements)
    Ya = Ya.reshape(num_elements)
    Thetaa = Thetaa.reshape(num_elements)
    dXr = dXr.reshape(num_elements)
    dYr = dYr.reshape(num_elements)
    dThetar = dThetar.reshape(num_elements)
    Xc = Xc.reshape(num_elements)
    Yc = Yc.reshape(num_elements)
    Thetac = Thetac.reshape(num_elements)
    Kprime = Kprime.reshape(num_elements)
    Bls = Bls.reshape(num_elements)

    d3X = d3X.reshape(num_elements)
    d3Y = d3Y.reshape(num_elements)
    Xf2 = Xf2.reshape(num_elements)
    Yf2 = Yf2.reshape(num_elements)
    Thetaf2 = Thetaf2.reshape(num_elements)
    S1 = S1.reshape(num_elements)
    feasible = feasible.reshape(num_elements)
    Bds = Bds.reshape(num_elements)

    indices = np.where(feasible >= 0)
    
    X = X[indices]
    dX = dX[indices]
    d2X = d2X[indices]
    Y = Y[indices]
    dY = dY[indices]
    d2Y = d2Y[indices]
    Xa = Xa[indices]
    Ya = Ya[indices]
    Thetaa = Thetaa[indices]
    dXr = dXr[indices]
    dYr = dYr[indices]
    dThetar = dThetar[indices]
    Xc = Xc[indices]
    Yc = Yc[indices]
    Thetac = Thetac[indices]
    Kprime = Kprime[indices]
    Bls = Bls[indices]

    d3X = d3X[indices]
    d3Y = d3Y[indices]
    Xf2 = Xf2[indices]
    Yf2 = Yf2[indices]
    Thetaf2 = Thetaf2[indices]
    S1 = S1[indices]
    feasible = feasible[indices]
    Bds = Bds[indices]

#! se for comparar a saída original da rede, passar essa parte para o final (antes de logdir)
Xa_next = Xa + (Xf2 - Xa) / S1
Ya_next = Ya + (Yf2 - Ya) / S1
Thetaa_next = Thetaa + (Thetaf2 - Thetaa) / S1
touch = (S1 == 1)
#! se for comparar a saída original da rede, passar essa parte para o final (antes de logdir)

X, Y = global_to_relative(X, Y, Xc, Yc, Thetac)
dX, dY = rotate(dX, dY, -Thetac)
d2X, d2Y = rotate(d2X, d2Y, -Thetac)
Xa, Ya = global_to_relative(Xa, Ya, Xc, Yc, Thetac)
Thetaa = Thetaa - Thetac
dXr, dYr = rotate(dXr, dYr, -Thetac)

#! se for comparar a saída original da rede, descomentar essa parte
# d3X, d3Y = rotate(d3X, d3Y, -Thetac)
# Xf2, Yf2 = global_to_relative(Xf2, Yf2, Xc, Yc, Thetac)
# Thetaf2 = Thetaf2 - Thetac
#! se for comparar a saída original da rede, descomentar essa parte

Y = invert_if_left_support(Y, Bls)
dY = invert_if_left_support(dY, Bls)
d2Y = invert_if_left_support(d2Y, Bls)
Ya = invert_if_left_support(Ya, Bls)
Thetaa = invert_if_left_support(Thetaa, Bls)
dYr = invert_if_left_support(dYr, Bls)
dThetar = invert_if_left_support(dThetar, Bls)


#! se for comparar a saída original da rede, descomentar essa parte
# d3Y = invert_if_left_support(d3Y, Bls)
# Yf2 = invert_if_left_support(Yf2, Bls)
# Thetaf2 = invert_if_left_support(Thetaf2, Bls)
#! se for comparar a saída original da rede, descomentar essa parte

#? a partir daqui para voltar para o início (depois de Bds[indices])

logdir = '../2023-03-02 00-55-53'
f = open(logdir + '/NN_info.json')
info = json.load(f)

model = load_model(logdir + '/imitationLearning', compile = False)
model.summary()

input_mean = info['input_mean']
input_std = info['input_std']

output_mean = info['output_mean']
output_std = info['output_std']

Xtrain, Ytrain, prediction = [], [], []

for i in range(len(X)):
    Xtrain.append([ X[i], dX[i], d2X[i], Y[i], dY[i], d2Y[i], Xa[i], Ya[i], Thetaa[i], dXr[i], dYr[i], dThetar[i], Kprime[i]]) #, Bds[i] 
    Ytrain.append([ d3X[i], d3Y[i], Xa_next[i], Ya_next[i], Thetaa_next[i], touch[i]])

Xtrain = np.array(Xtrain)
Ytrain = np.array(Ytrain)


for i in range(0,12):
    if input_std[i] !=0:
        Xtrain[:,i] = (Xtrain[:,i] - input_mean[i]) / input_std[i]

#! se for comparar a saída original da rede, descomentar essa parte
# for i in range(0,5):
#     if output_std[i] !=0:
#         Ytrain[:,i] = (Ytrain[:,i] - output_mean[i]) / output_std[i]
# Ytrain[:,5] = np.round(Ytrain[:,5])
#! se for comparar a saída original da rede, descomentar essa parte

inicio = 0
k = len(X)


for i in range(k):
    input = Xtrain[i+inicio,:]
    input = np.reshape(input, [np.size(input,  axis=0),1]).transpose()
    pred = model.predict(input)
    pred = np.hstack((pred[0], pred[1]))
    prediction.append(pred)


prediction = np.reshape(prediction, [k,6])
prediction[:,5] = np.round(prediction[:,5])

#! se for comparar a saída original da rede, comentar essa parte
for i in range(0,5):
    if output_mean[i] !=0:
        prediction[:,i] = (prediction[:,i] * output_std[i]) +  output_mean[i]
Ytrain[:,5] = np.round(Ytrain[:,5])

prediction[:,1] = invert_if_left_support(prediction[:,1], Bls)
prediction[:,3] = invert_if_left_support(prediction[:,3], Bls)
prediction[:,4] = invert_if_left_support(prediction[:,4], Bls)

prediction[:,0], prediction[:,1] = rotate(prediction[:,0], prediction[:,1], Thetac)
prediction[:,2], prediction[:,3] = rotate(prediction[:,2], prediction[:,3], Thetac)
prediction[:,2] = prediction[:,2] + Xc
prediction[:,3] = prediction[:,3] + Yc
prediction[:,4] = prediction[:,4] + Thetac
#! se for comparar a saída original da rede, comentar essa parte


inicio = 0
k = 200
for i in range(6):

        if i == 0:
            INPUT_STRING = 'd3X'
        elif i == 1:
            INPUT_STRING = 'd3Y'
        elif i == 2:
            INPUT_STRING = 'Xa_next'
        elif i == 3:
            INPUT_STRING = 'Ya_next'
        elif i == 4:
            INPUT_STRING = 'Thetaa_next'
        elif i == 5:
            INPUT_STRING = 'S1'

        plt.figure(constrained_layout=True)
        plt.rcParams.update({'font.family': 'sans-times', 'font.size': 16, 'font.sans-serif': 'Helvetica'})
        
        plt.plot(prediction[inicio:inicio+k, i], linewidth =2)
        plt.plot(Ytrain[inicio:inicio+k, i], linewidth =2)
        plt.xlabel('Timestep')
        if (INPUT_STRING == 'd3X') or (INPUT_STRING == 'd3Y'):
            plt.ylabel(r'Jerk $(m/s^2)$')
        elif (INPUT_STRING == 'Xa_next') or (INPUT_STRING == 'Ya_next'):
            plt.ylabel(r'Position $(m)$')
        elif (INPUT_STRING == 'Thetaa_next'):
            plt.ylabel(r'Angle $(rad)$')
        elif (INPUT_STRING == 'S1'):
            plt.ylabel('Decision (0 or 1)')
        plt.legend(['Neural Network', 'Expert (MPC)'], loc = 'upper left')
        for i in range (1, int(k/50)):
            plt.axvline(x=i*50, color= 'k', linestyle='dotted')
        if (INPUT_STRING == 'd3X'):
            plt.title(r'$\dddot x [k\vert k] $')
        elif (INPUT_STRING == 'd3Y'):
            plt.title(r'$\dddot y [k\vert k]$')
        elif (INPUT_STRING == 'Xa_next'):
            plt.title(r'$x_a[k+1]$')
        elif (INPUT_STRING == 'Ya_next'):
            plt.title(r'$y_a[k+1]$')
        elif (INPUT_STRING == 'Thetaa_next'):
            plt.title(r'$\Theta_a[k+1]$')
        elif (INPUT_STRING == 'S1'):
            plt.title(r'$b_{touch}$')
        plt.xlim(0, k)
        plt.savefig(f'{logdir}/{INPUT_STRING}.pdf', format='pdf')
        
plt.show()




