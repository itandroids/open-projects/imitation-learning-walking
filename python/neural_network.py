import numpy as np
import tensorflow as tf

from keras.models import Model, Input
from keras.layers import Dense, Dropout
import os
if (tf. __version__ == '1.5.0'):
    from keras.optimizers import Adam
else:
    from keras.optimizers import Adam
    # from tensorflow.keras.optimizers import Adam
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' #disable CPU AVX warning

ALPHA = None
    
class NeuralNetwork():
    def __init__(self, hyperparameters, X_train, y_train):
        self.hyperparameters = hyperparameters
        self.model = self.compile_model(X_train, X_train.shape[1], y_train.shape[1])

    def train(self, X_train, y_train, X_test, y_test, callback=tf.keras.callbacks.Callback()):
        batch_size = self.hyperparameters.batch_size
        lr_scheculer = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=0.000001, verbose = 1)
        early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=20, min_delta=0.0001, verbose = 1)
        return self.model.fit(X_train, [y_train[:,0:5], y_train[:,5:np.size(y_train, axis=1)]], epochs=int(self.hyperparameters.epochs),
                              validation_data=(X_test, [y_test[:,0:5], y_test[:,5:np.size(y_test, axis=1)]]), shuffle=True, verbose=1, 
                              callbacks = [callback, lr_scheculer, early_stopping], batch_size = batch_size) 

    def compile_model(self, X, input_size, out_size):
        global ALPHA
        nb_layers  = self.hyperparameters.nb_layers
        nb_neurons = self.hyperparameters.nb_neurons
        activation = self.hyperparameters.activation
        final_activation_regression = self.hyperparameters.final_activation_regression
        final_activation_classification = self.hyperparameters.final_activation_classification
        loss_regression = self.hyperparameters.loss_regression
        loss_classification = self.hyperparameters.loss_classification
        dropoput_rate = self.hyperparameters.dropout_rate
        ALPHA = float(self.hyperparameters.alpha_loss)

        input_layer = Input(shape=(input_size,), name = 'Input')
        layer = Dense(nb_neurons[0], activation = activation)(input_layer) #, kernel_regularizer=regularizers.L1L2(l1=1e-6, l2=1e-5)
        layer = Dropout(dropoput_rate)(layer)
        for layers in range(nb_layers - 1):
            layer = Dense(nb_neurons[layers+1], activation = activation)(layer) #, kernel_regularizer=regularizers.L1L2(l1=1e-6, l2=1e-5)
            layer = Dropout(dropoput_rate)(layer)

        output_regression = Dense(5, activation = final_activation_regression, name = 'Regression')(layer) #, kernel_regularizer=regularizers.L1L2(l1=1e-6, l2=1e-5)
        output_classification = Dense(out_size-5, activation = final_activation_classification, name = 'Classification')(layer) #, kernel_regularizer=regularizers.L1L2(l1=1e-6, l2=1e-5)

        model = Model(inputs = input_layer, outputs = [output_regression, output_classification])
        model.compile(optimizer=Adam(lr=self.hyperparameters.lr), loss=[loss_regression, loss_classification], loss_weights=[[1,1,1,1,1], ALPHA], metrics=['mae']) 
        model.summary()

        return model

    def evaluate_model(self, x_test, y_test):
        return self.model.evaluate(x_test, [y_test[:,0:5], y_test[:,5:np.size(y_test, axis=1)]], verbose=0)


    

